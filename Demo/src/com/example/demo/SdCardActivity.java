package com.example.demo;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class SdCardActivity extends MainActivity {

	private TextView path;
	private ListView listView;
	private Button btn_parent;
	private ImageView icon;
	private TextView file;

	// 记录当前父文件
	File currentParent;

	// 记录当前目录路径下的所有文件的文件组数

	File[] currentFiles;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.main);
		setContentView(R.layout.sdcard);

		path = (TextView) findViewById(R.id.path);
		listView = (ListView) findViewById(R.id.list);
		btn_parent = (Button) findViewById(R.id.btn_parent);
		icon = (ImageView) findViewById(R.id.icon);
		file = (TextView) findViewById(R.id.file);

		// 获取系统的SD卡目录

		File root = new File("/mnt/sdcard");

		// 要是SD卡存在

		if (root.exists()) {
			currentParent = root;
			currentFiles = root.listFiles();

			// 填充ListView
			inflateListView(currentFiles);
		}

		// 为listview绑定监听器

		listView.setOnItemClickListener(new OnItemClickListener() {

			public void OnItem(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// 用户单机了文件，直接返回，不处理任何；
				if (currentFiles[arg2].isFile()) {

				}

				//return;

				// 获取用户单机的文件夹下的所有文件

				File[] tmp = currentFiles[arg2].listFiles();
				if (tmp == null || tmp.length == 0) {
					Toast.makeText(SdCardActivity.this, "当前路径不可访问或者改路径下没有文件", 20000).show();
				} else {

					currentParent = currentFiles[arg2];

					currentFiles = tmp;

					inflateListView(currentFiles);
				}
			}

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub

			}

		});
		btn_parent.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					if (!currentParent.getCanonicalPath().equals("/mnt/sdcard")) {
						// 获取上级目录
						currentParent = currentParent.getParentFile();
						// 列出当前目录下所有文件
						currentFiles = currentParent.listFiles();
						// 再次更新ListView
						inflateListView(currentFiles);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
	}

	private void inflateListView(File[] files) {
		// TODO Auto-generated method stub
		
		//创建一个list集合！！！！list集合的元素是Map
		
	List<Map<String,Object>> listItem = new ArrayList<Map<String , Object>>();
	for(int i = 0; i < files.length; i++){  
        Map<String, Object> listItem1 = new HashMap<String, Object>();  
        //如果当前File是文件夹,使用floder图标；否则使用file图标  
        if(files[i].isDirectory()){  
            listItem1.put("icon", R.drawable.folder);  
        }  
        else{  
            listItem1.put("icon", R.drawable.folder);  
        }  
        listItem1.put("fileName", files[i].getName());  
        //添加List项  
        listItem.add(listItem1);  
    }  
	
	
	//创建一个SimpleAdapter
	
	SimpleAdapter simpleAdapter = new SimpleAdapter(this, listItem, R.layout.sdcard, new String[]{"icon","file"}, new int []{R.id.icon,R.id.file});
	
	
	
	//为ListView设置adapter
	
	listView.setAdapter(simpleAdapter);
	
	
	
	try{  
        path.setText("当前路径为: " + currentParent.getCanonicalPath());  
    }catch (Exception e) {  
        // TODO: handle exception  
        e.printStackTrace();  
	  
	}


	}

}
